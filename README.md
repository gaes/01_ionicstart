# IONIC3 #
Ionic is a complete open-source SDK for hybrid mobile app development.
The original version was released in 2013 and built on top of AngularJS and Apache Cordova.
The more recent release, known as Ionic 2 or simply "Ionic", are built on Angular. Ionic provides tools and services for developing hybrid mobile apps using Web technologies 
like CSS, HTML5, and Sass.


npm install -g cordova ionic

ionic start myApp tabs

cd myApp

ionic cordova platform add android

ionic serve --lab



https://ionicframework.com/getting-started/
https://ionicframework.com/docs/
https://ionicframework.com/docs/native/